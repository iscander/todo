FROM python:3.6-alpine as builder
RUN apk update && apk upgrade -fq && apk add  --no-cache --virtual .build-deps gcc musl-dev postgresql-dev git curl  && \
    addgroup -g 1000 django && adduser -D -G django -u 1000 django && \
    mkdir -p /todo && \
    chown -R django:django /home/django /todo 
WORKDIR /todo
USER django
ENV PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/home/django/.local/bin
RUN pip3 install --user git+https://github.com/pypa/pipenv.git && \
    git clone https://github.com/shacker/gtd.git  /todo && \
    rm -rf /todo/.git* /todo/project/local.example.py   && \
    pipenv --python 3.6 && pipenv install 
COPY local.py /todo/project/

FROM python:3.6-alpine
LABEL maintainer="Oleksandr Ohurtsov <iscander@gmail.com>"
COPY --from=builder /todo /todo
COPY --from=builder /home/django/.local /home/django/.local
RUN apk add  --no-cache libpq && \
    addgroup -g 1000 django && adduser -D -G django -u 1000 django && \
    chown -R django:django /home/django /todo 
WORKDIR /todo
USER django
ENV PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/home/django/.local/bin
EXPOSE 8000
ENTRYPOINT [ "pipenv", "run", "./manage.py" ]
# docker run --rm -it -p 8000:8000  -v $(pwd)/db.sqlite3:/todo/db.sqlite3 django-todo migrate
# docker run --rm -it -p 8000:8000  -v $(pwd)/db.sqlite3:/todo/db.sqlite3 django-todo hopper
# docker run --rm -it -p 8000:8000  -v $(pwd)/db.sqlite3:/todo/db.sqlite3 django-todo runserver 0.0.0.0:8000
