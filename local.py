# Overrides
from .settings import *  # noqa: F401

SECRET_KEY = os.getenv('SECRET','lksdf98wrhkjs88dsf8-324ksdm')

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE':   os.getenv('DB_ENGINE','django.db.backends.sqlite3'),
        'NAME':     os.getenv('DB_NAME',os.path.join(BASE_DIR, 'db.sqlite3')),
        'USER':     os.getenv('DB_USER','you'),
        'PASSWORD': os.getenv('DB_PASSWORD',''),
        'HOST':     os.getenv('DB_HOST','127.0.0.1'),
        'PORT':     os.getenv('DB_PORT',''),
    },
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
ALLOWED_HOSTS = ['*']
# TODO-specific settings
TODO_STAFF_ONLY = False
TODO_DEFAULT_LIST_SLUG = 'tickets'
TODO_DEFAULT_ASSIGNEE = None
TODO_PUBLIC_SUBMIT_REDIRECT = '/'