# Docker image

The todo application uses a Dockerimage for distribution. It is available as

`docker pull bodyring/django-todo`

- [Docker image](#docker-image)
  - [Info](#info)
  - [Local run](#local-run)

## Info

The [Dockerfile](../Dockerfile) available at the root of this repository. It uses two steps to build the image. On the first step it configure all the required dependencies and setup them for a specific user. The second step is for the final image. It leverage artifacts from the first build step and install minimum of required dependecies. Actualy only a one package with its dependencies had added for PostgreSQL client library.

The [local.py](../local.py) file is inhereted by the Docker image to setup the application configuration according environment variables. By default it is configured to run the application with SQLite database driver.

The image's entry point allows run only application specific command. You see them if you are running the container from the image.

```bash
$ docker run --rm -it bodyring/django-todo

Type 'manage.py help <subcommand>' for help on a specific subcommand.

Available subcommands:

[auth]
    changepassword
    createsuperuser

[contenttypes]
    remove_stale_contenttypes

[django]
    check
    compilemessages

<... CUTTED ... >
```

## Local run

You may try to run your own local copy of the application by default.

Create file `db.sqlite3` and directory `media/` inside your current directory. `mkdir media && touch db.sqlite3`

Now run following command.

`docker run --rm -it  -v $(pwd)/db.sqlite3:/todo/db.sqlite3 bodyring/django-todo migrate`

to create database structure. Output looks like

```bash
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, flatpages, sessions, sites, todo
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK

<... CUTTED ... >
```

next run

`docker run --rm -it  -v $(pwd)/db.sqlite3:/todo/db.sqlite3 bodyring/django-todo hopper`

to create random list and task data for a few fake users. The output is

`For each of two groups, created fake tasks in each of 5 fake lists.`

last step run

`docker run --rm -it  -v $(pwd)/db.sqlite3:/todo/db.sqlite3 -v $(pwd)/media:/todo/media -p 8000:8000 bodyring/django-todo`

to start the application service. The expected output is

```bash
runserver 0.0.0.0:8000
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
November 29, 2019 - 12:47:17
Django version 2.2.3, using settings 'project.local'
Starting development server at http://0.0.0.0:8000/
Quit the server with CONTROL-C.
```

Now the application available for your browser at http://localhost:8000