# Deployment

## Overview

This document describes sample TODO application deployment on kubernetes cluster. The application uses PostgreSQL bakend. The PosgreSQL backend is also deployed into the same kubernetes cluster. The host's (VirtualMachines) folders are providing persistency for both application and PostrgeSQL.

![Deployment overview](schema.png)

Actual deployment contains two deployments. One is for [PostrgeSQL](../deployments/postgre/) and second is for [application](../deployments/todo/) deployments. They are Helm charts which depends on shared common [configuration](../deployments/values.yaml).

## Database deployment

The PostgreSQL deployment contains:

 Name | Decription |
|---|---|
| postgre-db-config | contains system database credentials. They are `.Values.config.postgres` based environment variables.
| todo-nginx-config | contains initdb.sql script. Postgres container run the script in case of the database is empty. The script creates application's databse and user.

### Deployment

The `database-postgre` manages kubernetes deployment for the PostgreSQL service. It runs 1 replicaset for now. The deployment define a pod.

#### Pod

The pod contains Postgresql container. The container image configuration is defined at `.Values.postgres.image` The containers utilises `postgre-claim` mount volume for persistency.

### PersistentVolume

The `postgre-vol` is a PersistentVolume mapped to MiniKube's directory `/mnt/database`. It's purpose is to keep postgreSQL database files. There is `postgre-claim` PersistentVolumeClaim which is bound to  the `postgre-vol` for now. VolumeMount at a container refers to that claim rather then to a persistent volume directly.

### Service

There is an `pgdatabase`  ClusterIP  service which refers to the `database-postgre`. It makes the PostgreSQL available inside the cluster by its internal cluster name and IP. The service name is defined as `{{ .Values.config.app.DB_HOST }}`, service port is `{{ .Values.config.app.DB_PORT }}` it makes the service consistent with values which are passing to the application as environment variables DB credentials.

## Application deployment

The Application deployment contains:

### ConfigMap

| Name | Decription |
|---|---|
| todo-credentials | contains database credentials for the application
| todo-nginx-config | contains nginx sidecar container configuration. Map to `/etc/nginx/conf.d/nginx.conf`

### Deployment

The `application-todo` manages kubernetes deployment for the application. It runs 1 replicaset for now. The host directory based persitency is not safe for simultaneous access so scaling doesn't make sense for now. The deployment define a pod.

#### Pod and Job

The pod contains two container. The first one is a django `todo` application itself. The second is `todo-nginx` sidecar container. The last is responsible to serve static media files as an application components attachement. The containers share `media-claim` mount volume. The sidecar container has read only permission on it. It uses the application container as a backend for dynamic requests.

There is a conditional init container available. It's role is to apply database migration durring installation or upgrade. It doesn't start in case of installation with `demomode=true` parameter. That parameter initialize a Helm hook job to apply migrations and create random list and task data for a few fake users.

### PersistentVolume

The `media-vol` is a PersistentVolume mapped to MiniKube's directory `/mnt/media`. It's purpose is to keep application media files. There is `media-claim` PersistentVolumeClaim which is bound to  the `media-vol` for now. VolumeMount at a container refers to that claim rather then to a persistent volume directly.

### Service

There is an `application-todo`  ClusterIP  service which refers to the application. It makes the application available inside the cluster by its internal cluster name and IP. There is a command available to reach the application's service outside the cluster:
`$ kubectl port-forward svc/application-todo 9000:80`
It makes the application available at `http://127.0.0.1:9000`

### Ingress

The ingress `application-todo` is a better way to provide  an access to the application from outside a cluster. It maps application domain name  `django.to.do` to an external cluster IP. It is a `minikube ip` in case of a MiniKube deployment.
The domain name is defined at [values.yaml](../deployments/values.yaml) file as the only element of `.Values.ingress.hosts`

## Navigation

- [Requirements](./requirements.md) detailed requirements description.
- [Docker](./docker.md) describes application container in use.
- [Installation](./quickstart.md) describes application setup procedure.
- [Documentation](../README.md) documentation entry point. Describe the purpose of the project and contains the links to other documentations.