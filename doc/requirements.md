# Requirements

- [Requirements](#requirements)
  - [Components](#components)
  - [The known working configuration](#the-known-working-configuration)
  - [Navigation](#navigation)

This deployment has been tested on a local minikube installation. It could be applied to another configuration of Kubernetes cluster. It uses host's directory as a persistent volume storage. If your configuration doesn't allow to allocate a directory for kubernetes storage you are responsible to change appropriate setting for `app.volumes.volspec`, `app.volumes.claimspec`, `postgres.volumes.volspec` and `postgres.volumes.claimspec` accordingly at [values.yaml](../deployments/values.yaml) file.

## Components

- [**Minikube**](https://kubernetes.io/docs/tasks/tools/install-minikube/) a tool that runs a single-node Kubernetes cluster in a virtual machine on your personal computer.

- [**Helm**](https://helm.sh) the package manager for Kubernetes. Helm requires client and server side components until v3.0. The version 2.* is using here due v.3 is pretty fresh for nowadays. [Helm v.2+](https://github.com/helm/helm/releases/tag/v2.16.1) depends on `helm` on a client side and `tiller` on a cluster side. Check if it correctly set up on your Kubernetes cluster run `helm version`. There is a known issue with Helm v.2 installation over kubernetes v.1.16+. Refer to the [document](./tips.md) for the available workaround.
  
- [**kubectl**](https://kubernetes.io/docs/tasks/tools/install-kubectl/) a command line interface for running commands against Kubernetes clusters.

## The known working configuration

minikube version

```text
minikube version: v1.5.2
commit: 792dbf92a1de583fcee76f8791cff12e0c9440ad-dirty
```

kubectl version

```text
Client Version: version.Info{Major:"1", Minor:"14", GitVersion:"v1.14.8", GitCommit:"211047e9a1922595eaa3a1127ed365e9299a6c23", GitTreeState:"clean", BuildDate:"2019-10-15T12:11:03Z", GoVersion:"go1.12.10", Compiler:"gc", Platform:"darwin/amd64"}
Server Version: version.Info{Major:"1", Minor:"16", GitVersion:"v1.16.2", GitCommit:"c97fe5036ef3df2967d086711e6c0c405941e14b", GitTreeState:"clean", BuildDate:"2019-10-15T19:09:08Z", GoVersion:"go1.12.10", Compiler:"gc", Platform:"linux/amd64"}
```

helm version

```text
Client: &version.Version{SemVer:"v2.14.3", GitCommit:"0e7f3b6637f7af8fcfddb3d2941fcc7cbebb0085", GitTreeState:"clean"}
Server: &version.Version{SemVer:"v2.14.3", GitCommit:"0e7f3b6637f7af8fcfddb3d2941fcc7cbebb0085", GitTreeState:"clean"}
```

## Navigation

- [Deployment](./deployment.md) describes overall deployment configuration.
- [Installation](./quickstart.md) describes application setup procedure.
- [Documentation](../README.md) documentation entry point. Describe the purpose of the project and contains the links to other documentations.
