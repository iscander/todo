# Usefull tips

Colletion of usefull tips and tricks

- [Usefull tips](#usefull-tips)
  - [Install tiller](#install-tiller)

## Install tiller

There is a [known issue](https://github.com/helm/helm/issues/6374) exists when try to setup Helm's v.2.* cluster side service `tiller` on Kubernetes v.1.16*. The MiniKube v.1.4+ goes with Kubernetes 1.6+ by default. If you doesn't have `tiller` pre-installed and figured out the issue try to resolve it using provided helper.

`kubectl apply -f helpers/tiller.yaml`

The correct helm installation replies on `helm version` command looks like

```bash
Client: &version.Version{SemVer:"v2.14.3", GitCommit:"0e7f3b6637f7af8fcfddb3d2941fcc7cbebb0085", GitTreeState:"clean"}
Server: &version.Version{SemVer:"v2.14.3", GitCommit:"0e7f3b6637f7af8fcfddb3d2941fcc7cbebb0085", GitTreeState:"clean"}
```
