
# Quick start

- [Quick start](#quick-start)
  - [Requirements](#requirements)
  - [Installation](#installation)
    - [Host preparation](#host-preparation)
    - [Enable the Ingress controller](#enable-the-ingress-controller)
    - [Create deployment's namespace](#create-deployments-namespace)
    - [Install database](#install-database)
    - [Install application](#install-application)
    - [Run the application](#run-the-application)
  - [Navigation](#navigation)

The document explains step-by-step procedure how to setup the sample application inside MiniKube's k8s cluster. We assume the user is comfortable with *NIX system command line, has base knowledge and expirence in kubernetes CLI and helm tool.

## Requirements

This installation has been run inside MiniKube Kubernets cluster on MacOS based machine. It should be runable on a k8s cluster in other configuration but didn't tested. We assume the following tools installed and available for the user.

- **Minikube** a tool that runs a single-node Kubernetes cluster in a virtual machine on your personal computer.
- **Helm** v2.* the package manager for Kubernetes . Both client and server side `tiller` have to be installed.
- **kubectl** a command line interface for running commands against Kubernetes clusters.

The installation has been tested on the MiniKube configuration shown bellow. Keep attention it utileses MacOS's hypekit VM instead of VirtualBox. The VirtualBox's specific network configuration setting noted at appropriate place.

![MininKube configuration screen](minikube.png)

## Installation

The installation procedure assumes several steps you have to apply in the same order as they described in this document. We assume the whole repository has been downloaded and all referens in the instruction bellow are relative on repositori's root. The dependencies must be ment. You have prepare host (Virtual machine) first, next deploy database and application before try the application.

### Host preparation

It is requred to allocate a volumes on the host or virtual machine to keep the application state. There are two volumes required. The first one is for database files. The second for the application's media attachements files. Login to your host machine create those volumes, directories in our case, and set appropiate permissions. The example bellow is for minikube based cluster.

Login to miniKube node:
`minikube ssh`

Create directories and set the permissions:
`sudo  mkdir -p /mnt/{database,media} && sudo chmod 1777 /mnt/{database,media}`

Check those volumes:

```sh
$ ls -ld  /mnt/{database,media}
drwxrwxrwt 19  999 root 520 Nov 29 07:55 /mnt/database
drwxrwxrwt  3 root root  60 Nov 29 07:51 /mnt/media
```

![MiniKube volumes management](volumes.png)

### Enable the Ingress controller

To enable the NGINX Ingress controller, run the following command:
`minikube addons enable ingress`

Verify that the NGINX Ingress controller is running
`kubectl get pods -n kube-system  | grep nginx-ingress-controller`

The expected output looks like

```bash
nginx-ingress-controller-6fc5bcc8c9-r5t7m   1/1     Running   1          21h
```

**Note:** _This can take up to a minute._

### Create deployment's namespace

 By default the application has to be deployed under `django` kubernetes namespace. Create it if missing.

```bash
 kubectl create namespace django
```

If you are going to use another one, that means you know what you do. Change the intructions bellow accordingly.

### Install database

Run command bellow under deployments directory. It installs postgresql database service as helm's `database` release into `django` namespaces.

```bash
 helm upgrade -i database --namespace django  postgre/ -f values.yaml
```

The process will have finished with a screen looks like. It creates PostgreSQL database inside k8s cluster available by its internal ClusterIP.

```bash
Release "database" does not exist. Installing it now.
NAME:   database
LAST DEPLOYED: Fri Nov 29 09:49:18 2019
NAMESPACE: django
STATUS: DEPLOYED

RESOURCES:
==> v1/ConfigMap
NAME                     DATA  AGE
postgre-db-config        3     0s
postgre-db-init-scripts  1     0s

==> v1/Deployment
NAME              READY  UP-TO-DATE  AVAILABLE  AGE
database-postgre  0/1    1           0          0s

==> v1/PersistentVolume
NAME         CAPACITY  ACCESS MODES  RECLAIM POLICY  STATUS  CLAIM                 STORAGECLASS  REASON  AGE
postgre-vol  5Gi       RWX           Retain          Bound   django/postgre-claim  manual        0s

==> v1/PersistentVolumeClaim
NAME           STATUS  VOLUME       CAPACITY  ACCESS MODES  STORAGECLASS  AGE
postgre-claim  Bound   postgre-vol  5Gi       RWX           manual        0s

==> v1/Pod(related)
NAME                               READY  STATUS             RESTARTS  AGE
database-postgre-6cd4bd8f65-lbjj6  0/1    ContainerCreating  0         0s

==> v1/Service
NAME        TYPE       CLUSTER-IP    EXTERNAL-IP  PORT(S)   AGE
pgdatabase  ClusterIP  10.106.36.29  <none>       5432/TCP  0s


NOTES:
1.The Postgress database application up and running at:

      pgdatabase:5432
```

Wait while the specific pod is appearing with status `Running` before go to the next step.

```bash
 kubectl --namespace django get pods
NAME                                READY   STATUS    RESTARTS   AGE
database-postgre-6cd4bd8f65-xtdvr   1/1     Running   0          47m
```

### Install application

When your database is up and running inside the cluster you are ready to install the application itself.
Run command bellow under deployments directory. It installs `todo` application as helm's `application` release into `django` namespaces. The key `--set demomode=true` allows to fill up database with a random list and task data for a few fake users. As it has said it makes a sense only durring an installation for a demo purpose.

```bash
 helm upgrade -i application --namespace django todo/ -f values.yaml --set demomode=true
```

It takes a bit more time to set up the application then database installation. The post-setup screen also appears at the end of the process. To ensure the overall installation is complete run the command bellow you have to see something similiar to the picture.

```bash
$ kubectl --namespace django get pods,pv,ep,ing,job,svc
NAME                                    READY   STATUS    RESTARTS   AGE
pod/application-todo-84d57f8b77-8pm5g   2/2     Running   0          44s
pod/database-postgre-6cd4bd8f65-xtdvr   1/1     Running   0          15m

NAME                           CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                  STORAGECLASS   REASON   AGE
persistentvolume/media-vol     1Gi        RWX            Retain           Bound    django/media-claim     manual                  44s
persistentvolume/postgre-vol   5Gi        RWX            Retain           Bound    django/postgre-claim   manual                  15m

NAME                         ENDPOINTS         AGE
endpoints/application-todo   172.17.0.8:80     44s
endpoints/pgdatabase         172.17.0.6:5432   15m

NAME                                  HOSTS          ADDRESS        PORTS   AGE
ingress.extensions/application-todo   django.to.do   192.168.64.2   80      44s

NAME                       TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/application-todo   ClusterIP   10.99.149.87    <none>        80/TCP     44s
service/pgdatabase         ClusterIP   10.102.87.146   <none>        5432/TCP   15m

```

### Run the application

The screen above contains important information you have to apply before check the application at a browser.

```bash
 HOSTS          ADDRESS
 django.to.do   192.168.64.2
```

It describes the domain name for the application and associated IP address. Update your `/etc/hosts` records with appropriate values. Add the following line to the bottom of the /etc/hosts file.

**Note:** _If you are running Minikube locally as VirtualBox machine, use `minikube ip` to get the external IP. The IP address displayed within the ingress list will be the internal IP._

```192.168.64.2 django.to.do```

This sends requests from [django.to.do](http://django.to.do) to Minikube. It is time to check the application in your browser. Open the browser and type `http://django.to.do` in the address bar. You have to see:

![Browser window](browser.png)

If you have install the application with `--set demomode=true` then all the option shown at the start screen are available:

You can log into this demo site as one of several users, who have different permissions and group memberships.

- To access todo lists for group *Basket Weavers*, log in as **user1** or **user2**
- To access todo lists for group *Scuba Divers*, log in as **user3** or **user4**
- To access all lists, and to be able to create new lists, login as user **staffer**
- The password for all demo users is _**todo**_

Enjoy your application.

## Navigation

- [Deployment](./deployment.md) describes overall deployment configuration.
- [Requirements](./requirements.md) detailed requirements description.
- [Docker](./docker.md) describes application container in use.
- [Documentation](../README.md) documentation entry point. Describe the purpose of the project and contains the links to other documentations.
