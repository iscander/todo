# TODO deployment on MiniKube

*Authored by Oleksandr Ohurtsov <iscander@gmail.com>*

- [TODO deployment on MiniKube](#todo-deployment-on-minikube)
  - [Documentation](#documentation)
  - [Repository structure](#repository-structure)

 The repository contains a sample of deployment of TODO application in kubernetes environment.
It is mostly PoW where application and PostgreSQL has been deployed  into kubernetes using configuration as code technique.
The sample utilises MiniKube cluster on a local environment to deploy both PostgreSQL and the application itself into the same kubernetes cluster.
Refer to the following documentation for additional information.

![Browser window](doc/browser.png)

## Documentation

- [Quick start guide](doc/quickstart.md) step by step instruction how to deploy the application.
- [Requirements](doc/requirements.md) expecting tools to be set up.
- [Deployment](doc/deployment.md) describes overall deployment overview.
- [Docker image](doc/docker.md) Docker image in use description.
- [Tips](doc/tips.md) usefull tips and tricks. Explain how to resolve `tiller`'s installation issue.

## Repository structure

| Entry | Description |
|---|---|
 [Dockerfile](Dockerfile) | A Docker file to build the application image. More details are available at [Docker image](doc/docker.md) document
| [local.py](local.py) | overides default application configuration file. Allows to accepts settings from environment variables
| [doc/](doc) | varius documentation available from the [links above](#documentation)
| [deployments/](deployments) | the directory contains Helm charts to deploy all the application components.
| [deployments/values.yaml](deployments/values.yaml) | shared configuration values across database and application deployments
| [deployments/postgre](deployments/postgre) | Helm chart to deploy PostgreSQL database
| [deployments/todo](deployments/todo) | Helm chart to deploy TODO application
| [helpers/](helpers) | the directory contains a Kubernetes scenario which allows to set up Helm's tiller into Kubernetes 1.16+ cluster. Please refer to [Tips](doc/tips.md) document for details.
